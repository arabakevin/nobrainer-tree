$:.push File.expand_path('lib', __dir__)


# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'nobrainer-tree'
  spec.version     = '0.0.2'
  spec.authors     = ['Guillaume Hain']
  spec.email       = ['zedtux@zedroot.org']
  spec.homepage    = 'https://gitlab.com/zedtux/nobrainer-tree'
  spec.summary     = 'A tree structure for NoBrainer documents using the materialized path pattern'
  spec.description = 'A tree structure for NoBrainer documents using the materialized path pattern'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata = {
      'allowed_push_host' => 'https://rubygems.org',
      'bug_tracker_uri'   => 'https://gitlab.com/zedtux/nobrainer-tree/issues',
      'source_code_uri'   => 'https://gitlab.com/zedtux/nobrainer-tree',
      'changelog_uri'     => 'https://gitlab.com/zedtux/nobrainer-tree/master/CHANGELOG.md'
    }
  else
    raise 'RubyGems 2.0 or newer is required to protect against public gem ' \
          'pushes.'
  end

  spec.files = Dir[]

  spec.test_files = Dir["spec/**/*"]

  spec.required_ruby_version = '>= 2.2.0'

  spec.add_dependency 'nobrainer', '>= 0'

  spec.add_development_dependency 'rake', '~> 0.9.2'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'yard', '~> 0.8'
end
